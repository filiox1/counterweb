package com.qaagility.controller;
import static org.junit.Assert.*;
import org.junit.Test;

public class CalculatorTest {
    @Test
    public void testAdd() throws Exception {

        int k= new Calculator().add();
        assertEquals("Add", 9, k);

    }
    @Test
    public void testD1() throws Exception {
	 int k= new CountDivision().division(2,0);
        assertEquals("Division", Integer.MAX_VALUE, k);
	
    }
    @Test
    public void testD2() throws Exception {
	int k= new CountDivision().division(4,2);
        assertEquals("Division2", 2, k);

    }

}

